package model

type Registration struct {
	PassportNumber string `json:"passportNumber"`
	RoomNumber     string `json:"roomNumber"`
	CheckIn        string `json:"checkIn"`
	CheckOut       string `json:"checkOut"`
}
