package model

import (
	"gitlab.com/guap-programming/2/saod/kp/avl"
	"gitlab.com/guap-programming/2/saod/kp/helpers"
)

type HotelRoom struct {
	Number    string `json:"number"`
	Places    int    `json:"places"`
	Bedrooms  int    `json:"bedrooms"`
	Bathroom  bool   `json:"bathroom"`
	Utilities string `json:"utilities"`
}

func (h *HotelRoom) Less(key avl.Key) bool {
	k1, _ := helpers.HotelNumberToInt(h.Number)
	k2, _ := helpers.HotelNumberToInt(key.(*HotelRoom).Number)
	return k1 < k2
}

func (h *HotelRoom) Eq(key avl.Key) bool {
	k1, _ := helpers.HotelNumberToInt(h.Number)
	k2, _ := helpers.HotelNumberToInt(key.(*HotelRoom).Number)

	return k1 == k2
}
