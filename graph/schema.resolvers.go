package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"gitlab.com/guap-programming/2/saod/kp/errors"
	"strings"
	"time"

	"gitlab.com/guap-programming/2/saod/kp/avl"
	"gitlab.com/guap-programming/2/saod/kp/bms"
	"gitlab.com/guap-programming/2/saod/kp/graph/generated"
	"gitlab.com/guap-programming/2/saod/kp/graph/model"
	"gitlab.com/guap-programming/2/saod/kp/hashtable"
	"gitlab.com/guap-programming/2/saod/kp/helpers"
	"gitlab.com/guap-programming/2/saod/kp/list"
)

func (r *hotelRoomResolver) Visitors(_ context.Context, obj *model.HotelRoom) ([]*model.Visitor, error) {
	visitors := make([]*model.Visitor, 0)
	for reg := r.registrations.Front(); reg != nil; reg = reg.Next() {
		if reg.Value.(*model.Registration).RoomNumber == obj.Number {
			visitorPassport := reg.Value.(*model.Registration).PassportNumber
			hashKey, _ := helpers.PassportNumberToInt(visitorPassport)
			_, visitor := r.visitors.Get(hashKey)

			visitors = append(visitors, visitor.(*model.Visitor))
		}
	}

	return visitors, nil
}

func (r *mutationResolver) CreateVisitor(_ context.Context, input model.NewVisitor) (*model.Visitor, error) {
	hashKey, err := helpers.PassportNumberToInt(input.PassportNumber)
	if err != nil {
		return nil, err
	}

	visitor := &model.Visitor{
		PassportNumber: input.PassportNumber,
		FullName:       input.FullName,
		BirthYear:      input.BirthYear,
		Reason:         input.Reason,
	}
	r.visitors.Put(hashKey, visitor)
	r.visitors.Display()
	return visitor, nil
}

func (r *mutationResolver) RemoveVisitor(_ context.Context, passportNumber string) (bool, error) {
	hashKey, err := helpers.PassportNumberToInt(passportNumber)
	if err != nil {
		return false, err
	}
	res := r.visitors.Del(hashKey)
	r.visitors.Display()

	return res, nil
}

func (r *mutationResolver) RemoveAllVisitors(_ context.Context) (bool, error) {
	r.visitors = hashtable.CreateHashTable()
	r.visitors.Display()
	return true, nil
}

func (r *mutationResolver) CreateHotelRoom(_ context.Context, input model.NewHotelRoom) (*model.HotelRoom, error) {
	_, err := helpers.HotelNumberToInt(input.Number)
	if err != nil {
		return nil, err
	}

	hr := &model.HotelRoom{
		Number:    input.Number,
		Places:    input.Places,
		Bedrooms:  input.Bedrooms,
		Bathroom:  input.Bathroom,
		Utilities: input.Utilities,
	}

	avl.Insert(&r.hotelRooms, hr)
	avl.Display(r.hotelRooms)
	return hr, nil
}

func (r *mutationResolver) RemoveHotelRoom(_ context.Context, number string) (bool, error) {
	_, err := helpers.HotelNumberToInt(number)
	if err != nil {
		return false, err
	}

	res := avl.Remove(&r.hotelRooms, &model.HotelRoom{Number: number})
	avl.Display(r.hotelRooms)
	return res, nil
}

func (r *mutationResolver) RemoveAllHotelRooms(_ context.Context) (bool, error) {
	r.hotelRooms = nil
	return true, nil
}

func (r *mutationResolver) CreateRegistration(_ context.Context, input model.NewRegistration) (*model.Registration, error) {
	res, err := prepareRegistration(input, r)
	if err != nil {
		return nil, err
	}

	// if this registration exists, update it
	for reg := r.registrations.Front(); reg != nil; reg = reg.Next() {
		if reg.Value.(*model.Registration).PassportNumber == input.PassportNumber &&
			reg.Value.(*model.Registration).RoomNumber == input.RoomNumber {

			reg.Value = res

			return res, nil
		}
	}

	free, err := checkForFree(r.Resolver, res)
	if err != nil {
		return nil, err
	}
	if !free {
		return nil, fmt.Errorf("hotel room №%s is busy", input.RoomNumber)
	}

	r.registrations.PushFront(res)

	r.registrations.Sort(func(i *list.Element, j *list.Element) bool {
		indexI, _ := helpers.HotelNumberToInt(i.Value.(*model.Registration).RoomNumber)
		indexJ, _ := helpers.HotelNumberToInt(j.Value.(*model.Registration).RoomNumber)
		return indexI < indexJ
	})
	return res, nil
}

func checkForFree(r *Resolver, registration *model.Registration) (bool, error) {
	var hr *model.HotelRoom
	avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
		if strings.Contains(node.Data.(*model.HotelRoom).Number, registration.RoomNumber) {
			hr = node.Data.(*model.HotelRoom)
		}
	})

	registrations := make([]*model.Registration, 0, r.registrations.Len())
	for reg := r.registrations.Front(); reg != nil; reg = reg.Next() {
		if reg.Value.(*model.Registration).RoomNumber == registration.RoomNumber {
			eCI, err := helpers.StringToTime(reg.Value.(*model.Registration).CheckIn, helpers.DateFormat)
			if err != nil {
				return false, err
			}
			eCO, _ := helpers.StringToTime(reg.Value.(*model.Registration).CheckOut, helpers.DateFormat)

			rCI, err := helpers.StringToTime(registration.CheckIn, helpers.DateFormat)
			if err != nil {
				return false, err
			}

			rCO, _ := helpers.StringToTime(registration.CheckOut, helpers.DateFormat)
			//check registration for crossing time with existing registrations
			if (rCI.After(eCI) && (eCO.IsZero() || rCI.Before(eCO))) ||
				(!rCO.IsZero() && rCO.After(eCI) && (eCO.IsZero() || rCO.Before(eCO))) {
				registrations = append(registrations, reg.Value.(*model.Registration))
			}
		}
	}

	return hr.Places > len(registrations), nil

}

func prepareRegistration(input model.NewRegistration, r *mutationResolver) (*model.Registration, error) {
	hashKey, err := helpers.PassportNumberToInt(input.PassportNumber)
	if err != nil {
		return nil, err
	}

	ok, _ := r.visitors.Get(hashKey)
	if !ok {
		return nil, errors.PassportNumberNotFound
	}

	var hr *model.HotelRoom
	avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
		if strings.Contains(node.Data.(*model.HotelRoom).Number, input.RoomNumber) {
			hr = node.Data.(*model.HotelRoom)
		}
	})
	if hr == nil {
		return nil, errors.HotelRoomNotFound
	}

	var checkIn string
	var checkOut string
	if input.CheckIn != nil {
		_, err = time.Parse("02-01-2006", *input.CheckIn)
		if err != nil {
			return nil, err
		}
		checkIn = *input.CheckIn
	}
	if input.CheckOut != nil {
		_, err = time.Parse("02-01-2006", *input.CheckOut)
		if err != nil {
			return nil, err
		}

		checkOut = *input.CheckOut
	}

	res := &model.Registration{
		PassportNumber: input.PassportNumber,
		RoomNumber:     input.RoomNumber,
		CheckIn:        checkIn,
		CheckOut:       checkOut,
	}
	return res, nil
}

func (r *queryResolver) Visitors(_ context.Context, fullName *string) ([]*model.Visitor, error) {
	allRecords := r.visitors.GetAllRecords()
	res := make([]*model.Visitor, 0, len(allRecords))
	for _, record := range allRecords {
		v := record.(*model.Visitor)
		if fullName != nil && v.FullName != *fullName {
			continue
		}
		res = append(res, record.(*model.Visitor))
	}

	return res, nil
}

func (r *queryResolver) Visitor(_ context.Context, passportNumber string) (*model.Visitor, error) {
	hashKey, err := helpers.PassportNumberToInt(passportNumber)
	if err != nil {
		return nil, err
	}

	ok, v := r.visitors.Get(hashKey)
	if !ok {
		return nil, errors.PassportNumberNotFound
	}
	return v.(*model.Visitor), nil
}

func (r *queryResolver) HotelRooms(_ context.Context, utility *string) ([]*model.HotelRoom, error) {
	hotelRooms := make([]*model.HotelRoom, 0, 8)
	avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
		if utility != nil && bms.Search(node.Data.(*model.HotelRoom).Utilities, *utility) == 0 {
			return
		}
		hotelRooms = append(hotelRooms, node.Data.(*model.HotelRoom))
	})

	return hotelRooms, nil
}

func (r *queryResolver) HotelRoom(_ context.Context, number string) (*model.HotelRoom, error) {
	var res *model.HotelRoom
	avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
		if strings.Contains(node.Data.(*model.HotelRoom).Number, number) {
			res = node.Data.(*model.HotelRoom)
		}
	})
	if res != nil {
		return res, nil
	}
	return nil, errors.HotelRoomNotFound
}

func (r *registrationResolver) Visitor(_ context.Context, obj *model.Registration) (*model.Visitor, error) {
	hashKey, err := helpers.PassportNumberToInt(obj.PassportNumber)
	if err != nil {
		return nil, err
	}

	ok, v := r.visitors.Get(hashKey)
	if !ok {
		return nil, errors.PassportNumberNotFound
	}

	return v.(*model.Visitor), nil
}

func (r *registrationResolver) HotelRoom(_ context.Context, obj *model.Registration) (*model.HotelRoom, error) {
	var res *model.HotelRoom
	avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
		if strings.Contains(node.Data.(*model.HotelRoom).Number, obj.RoomNumber) {
			res = node.Data.(*model.HotelRoom)
		}
	})
	if res != nil {
		return res, nil
	}
	return nil, errors.HotelRoomNotFound
}

func (r *visitorResolver) HotelRoom(_ context.Context, obj *model.Visitor) ([]*model.HotelRoom, error) {
	rooms := make([]*model.HotelRoom, 0)
	for reg := r.registrations.Front(); reg != nil; reg = reg.Next() {
		if reg.Value.(*model.Registration).PassportNumber == obj.PassportNumber {
			var res *model.HotelRoom
			avl.PreOrder(r.hotelRooms, func(node *avl.Node) {
				if strings.Contains(node.Data.(*model.HotelRoom).Number, reg.Value.(*model.Registration).RoomNumber) {
					res = node.Data.(*model.HotelRoom)
				}
			})
			rooms = append(rooms, res)
		}
	}

	return rooms, nil
}

// HotelRoom returns generated.HotelRoomResolver implementation.
func (r *Resolver) HotelRoom() generated.HotelRoomResolver { return &hotelRoomResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Registration returns generated.RegistrationResolver implementation.
func (r *Resolver) Registration() generated.RegistrationResolver { return &registrationResolver{r} }

// Visitor returns generated.VisitorResolver implementation.
func (r *Resolver) Visitor() generated.VisitorResolver { return &visitorResolver{r} }

type hotelRoomResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type registrationResolver struct{ *Resolver }
type visitorResolver struct{ *Resolver }
