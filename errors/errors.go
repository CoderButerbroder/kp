package errors

import (
	"errors"
	"fmt"
)

func WrongPassportNumberError(passportNumber string) error {
	return fmt.Errorf("wrong passport format: expected ^[0-9]{4}-[0-9]{6}$, got: %s", passportNumber)
}

func WrongHotelNumberError(hotelNumber string) error {
	return fmt.Errorf("wrong passport format: expected ^[ЛПОМ][0-9]{3}$, got: %s", hotelNumber)
}

var (
	PassportNumberNotFound = errors.New("passport number not found")
)

var (
	HotelRoomNotFound = errors.New("hotel room not found")
)
